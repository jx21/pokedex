package jonathan.epn.pokedex

import android.text.style.LineHeightSpan

data class PokedexResponse(
    val count: Int,
    val results: List<PokemonResponse>

){
    data class PokemonResponse(
        val name: String,
        val url:String,
        val id: Int,
        val height: Int,
        val weight: Int
    )
}